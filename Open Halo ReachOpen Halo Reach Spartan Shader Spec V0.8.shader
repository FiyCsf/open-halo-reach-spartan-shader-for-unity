// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Open Halo Reach/Open Halo Reach Spartan Shader Spec V0.8"
{
	Properties
	{
		_MainTex("Diffuse", 2D) = "white" {}
		_ControlMap("Control Map", 2D) = "black" {}
		[Normal]_BumpMap("NormalMap", 2D) = "bump" {}
		[Toggle]_HaloReachDirectXNormalMap("Halo Reach (Direct X) Normal Map?", Float) = 0
		_SpecularMultiplier("Specular Multiplier", Range( 0 , 5)) = 1
		_SmoothnessMultiplier("Smoothness Multiplier ", Range( 0 , 2)) = 1
		_Color("Colour Red Channel", Color) = (1,0,0,0)
		_ColourGreenChannel("Colour Green Channel", Color) = (0.07799447,1,0,0)
		[Toggle]_ADDFakeZone3InverseofRGChannels("[ADD] Fake Zone 3? [Inverse of R&G Channels]", Float) = 1
		[HDR]_FakeZone3Colour("Fake Zone 3 Colour", Color) = (1,1,1,0)
		_EmissionMap("[ADD] Emission Texture:", 2D) = "black" {}
		_EmissionColor("Emission Color", Color) = (0,0,0,0)
		_ADDRedDetailTexture("[ADD] Red Detail Texture:", 2D) = "white" {}
		_ADDGreenDetailTexture("[ADD] Green Detail Texture:", 2D) = "white" {}
		_ADDFakezone3DetailTexture("[ADD] Fake zone 3  Detail Texture:", 2D) = "white" {}
		_OcclusionMap("Occlusion Map", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityCG.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		uniform float _HaloReachDirectXNormalMap;
		uniform sampler2D _BumpMap;
		uniform float4 _BumpMap_ST;
		SamplerState sampler_BumpMap;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform sampler2D _ControlMap;
		SamplerState sampler_ControlMap;
		uniform float4 _ControlMap_ST;
		uniform float4 _Color;
		uniform sampler2D _ADDRedDetailTexture;
		uniform float4 _ADDRedDetailTexture_ST;
		uniform float4 _ColourGreenChannel;
		uniform sampler2D _ADDGreenDetailTexture;
		uniform float4 _ADDGreenDetailTexture_ST;
		uniform float _ADDFakeZone3InverseofRGChannels;
		uniform float4 _FakeZone3Colour;
		uniform sampler2D _ADDFakezone3DetailTexture;
		uniform float4 _ADDFakezone3DetailTexture_ST;
		uniform sampler2D _EmissionMap;
		uniform float4 _EmissionMap_ST;
		uniform float4 _EmissionColor;
		SamplerState sampler_MainTex;
		uniform float _SpecularMultiplier;
		uniform float _SmoothnessMultiplier;
		uniform sampler2D _OcclusionMap;
		uniform float4 _OcclusionMap_ST;

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float2 uv_BumpMap = i.uv_texcoord * _BumpMap_ST.xy + _BumpMap_ST.zw;
			float3 tex2DNode7 = UnpackNormal( tex2D( _BumpMap, uv_BumpMap ) );
			float3 appendResult14 = (float3(tex2DNode7.r , -tex2DNode7.g , tex2DNode7.b));
			o.Normal = (( _HaloReachDirectXNormalMap )?( appendResult14 ):( tex2DNode7 ));
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 tex2DNode6 = tex2D( _MainTex, uv_MainTex );
			float4 Diffuse68 = tex2DNode6;
			float2 uv_ControlMap = i.uv_texcoord * _ControlMap_ST.xy + _ControlMap_ST.zw;
			float4 tex2DNode1 = tex2D( _ControlMap, uv_ControlMap );
			float ControlR67 = tex2DNode1.r;
			float2 uv_ADDRedDetailTexture = i.uv_texcoord * _ADDRedDetailTexture_ST.xy + _ADDRedDetailTexture_ST.zw;
			float4 colorZone1Detail166 = tex2D( _ADDRedDetailTexture, uv_ADDRedDetailTexture );
			float ControlG73 = tex2DNode1.g;
			float2 uv_ADDGreenDetailTexture = i.uv_texcoord * _ADDGreenDetailTexture_ST.xy + _ADDGreenDetailTexture_ST.zw;
			float4 ColorZone2Detail168 = tex2D( _ADDGreenDetailTexture, uv_ADDGreenDetailTexture );
			float3 linearToGamma527 = LinearToGammaSpace( tex2DNode1.rgb );
			float3 break528 = linearToGamma527;
			float ControlRgamma535 = break528.x;
			float ControlGgamma536 = break528.y;
			float clampResult533 = clamp( ( ControlRgamma535 + ControlGgamma536 ) , 0.0 , 1.0 );
			float temp_output_526_0 = ( 1.0 - clampResult533 );
			float4 temp_cast_2 = (temp_output_526_0).xxxx;
			float2 uv_ADDFakezone3DetailTexture = i.uv_texcoord * _ADDFakezone3DetailTexture_ST.xy + _ADDFakezone3DetailTexture_ST.zw;
			float4 ColorZone3Detail170 = tex2D( _ADDFakezone3DetailTexture, uv_ADDFakezone3DetailTexture );
			float4 blendOpSrc569 = Diffuse68;
			float4 blendOpDest569 = ( ( ( ControlR67 * ( _Color * colorZone1Detail166 ) ) + ( ControlG73 * ( _ColourGreenChannel * ColorZone2Detail168 ) ) ) + (( _ADDFakeZone3InverseofRGChannels )?( ( temp_output_526_0 * ( _FakeZone3Colour * ColorZone3Detail170 ) ) ):( temp_cast_2 )) );
			float4 temp_cast_3 = (( 0.5 + 0.5 )).xxxx;
			float4 blendOpSrc658 = ( saturate( ( blendOpSrc569 * blendOpDest569 ) ));
			float4 blendOpDest658 = temp_cast_3;
			float4 FiysColourMethod97 = ( saturate( ( blendOpSrc658 * blendOpDest658 ) ));
			o.Albedo = FiysColourMethod97.rgb;
			float2 uv_EmissionMap = i.uv_texcoord * _EmissionMap_ST.xy + _EmissionMap_ST.zw;
			o.Emission = ( tex2D( _EmissionMap, uv_EmissionMap ) * _EmissionColor ).rgb;
			float3 temp_cast_6 = (tex2DNode6.a).xxx;
			float3 temp_cast_7 = (tex2DNode6.a).xxx;
			float3 linearToGamma664 = LinearToGammaSpace( temp_cast_7 );
			float3 DiffuseAlphaGamma665 = linearToGamma664;
			float DiffuseAlpha662 = tex2DNode6.a;
			float temp_output_676_0 = ( DiffuseAlpha662 * DiffuseAlpha662 );
			float temp_output_681_0 = ( temp_output_676_0 * temp_output_676_0 * temp_output_676_0 );
			float4 clampResult571 = clamp( saturate( ( ( float4( DiffuseAlphaGamma665 , 0.0 ) * ( ( ( FiysColourMethod97 * float4( 1.949779,1.949779,1.949779,0 ) ) * DiffuseAlpha662 ) + ( temp_output_681_0 * 0.25 ) ) ) * _SpecularMultiplier ) ) , float4( 0.05,0.05,0.05,0 ) , float4( 1,1,1,0 ) );
			float4 VarSpecular428 = clampResult571;
			o.Specular = VarSpecular428.rgb;
			float clampResult570 = clamp( ( ( DiffuseAlpha662 * _SmoothnessMultiplier ) - temp_output_681_0 ) , 0.0 , 1.0 );
			float VarSmoothness429 = clampResult570;
			o.Smoothness = VarSmoothness429;
			float temp_output_693_0 = ( tex2DNode7.b * tex2DNode7.b * tex2DNode7.b );
			float clampResult705 = clamp( temp_output_693_0 , 0.75 , 1.0 );
			float2 uv_OcclusionMap = i.uv_texcoord * _OcclusionMap_ST.xy + _OcclusionMap_ST.zw;
			float4 tex2DNode699 = tex2D( _OcclusionMap, uv_OcclusionMap );
			float4 VarAmbientOcclusion697 = ( ( clampResult705 * tex2DNode699 ) * tex2DNode699 );
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float fresnelNdotV707 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode707 = ( 0.0 + 0.33 * pow( 1.0 - fresnelNdotV707, 5.0 ) );
			float4 clampResult712 = clamp( saturate( ( VarAmbientOcclusion697 * ( 1.0 - fresnelNode707 ) ) ) , float4( 0.05,0.05,0.05,0 ) , float4( 1,1,1,0 ) );
			o.Occlusion = clampResult712.r;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardSpecular keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandardSpecular o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardSpecular, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Standard (Specular setup)"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18600
0;0;2560;1378;1174.343;2779.024;2.205522;True;True
Node;AmplifyShaderEditor.CommentaryNode;57;-496,-1536;Inherit;False;5743.567;2685.474;Colour Zones Metallics and Smoothness are all contained within the Control File. Red is Colour Zone 1, Green 2, Blue is the Halo 5 Metallic, And Alpha is the Halo 5 Smoothness;27;68;6;610;609;125;536;535;528;527;1;65;226;114;662;664;665;736;740;739;737;738;741;742;743;746;744;745;Diffuse & Control: Your Colouring, Specular and Smoothness Data;0,0.3379312,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;1;-481.0926,-1296;Inherit;True;Property;_ControlMap;Control Map;1;0;Create;True;0;0;False;0;False;-1;None;ada1dd75c8e115d45a5de256b2021c0c;True;0;False;black;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LinearToGammaNode;527;-480,-1088;Inherit;True;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;745;-240,-880;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;744;-240,-880;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;746;-480,-816;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;743;-480,-816;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;65;-465,-576;Inherit;False;4641.533;1704.518;Fiy's Colouring Method, with Detail textures:        Using the Control, and Defuse, Let's Get some Armor Colours Going                                 More subtle then inital method, prevents blown colours.;22;128;569;380;660;213;211;205;82;590;81;594;64;592;591;488;489;606;604;116;605;607;126;Colouring: The most visable part of the Shader;0.3529412,0.8929005,1,1;0;0
Node;AmplifyShaderEditor.BreakToComponentsNode;528;-432,-784;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.CommentaryNode;226;832,-1424;Inherit;False;664.4159;777.4517;Registering Detail Albedos;8;331;332;170;171;168;167;166;165;Detail Albedos;0.9926471,0.175173,0.93627,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;126;207,640;Inherit;False;2261.492;419.854;Here the Red and Green Of the Control are Added, and then inverted, to create us the 3rd zone of the Armor's Colouring   This is fake, and should not be considered a 'correct' option;11;96;94;533;534;526;85;208;207;174;120;661;THERE IS NO DEFAULT 3rd COLOUR ZONE;1,0.8482759,0,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;536;16,-784;Float;True;ControlGgamma;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;535;-160,-976;Float;True;ControlRgamma;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;171;864,-1040;Inherit;True;Property;_ADDFakezone3DetailTexture;[ADD] Fake zone 3  Detail Texture:;14;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;208;527,688;Inherit;True;535;ControlRgamma;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;207;751,768;Inherit;True;536;ControlGgamma;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;170;1200,-1040;Float;True;ColorZone3Detail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;85;975,688;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;120;239,688;Float;False;Property;_FakeZone3Colour;Fake Zone 3 Colour;9;1;[HDR];Create;True;0;0;False;0;False;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;533;1183,688;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;174;224,864;Inherit;True;170;ColorZone3Detail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;526;1439,688;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;661;576,944;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;94;1887,832;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;96;2143,736;Float;True;Property;_ADDFakeZone3InverseofRGChannels;[ADD] Fake Zone 3? [Inverse of R&G Channels];8;0;Create;True;0;0;False;0;False;1;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;125;272,-1168;Inherit;False;543.4978;576.2533;Spliting the Control into channels as intended for use;17;67;734;728;727;279;280;123;121;729;730;611;608;733;732;731;73;735;The Control Split;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;607;2479,768;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;605;2495,768;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;733;352,-912;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;734;352,-912;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;6;-480,-1488;Inherit;True;Property;_MainTex;Diffuse;0;0;Create;False;0;0;False;0;False;-1;None;05a515e7db2eb9b49b62799c324c2a6e;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;604;2495,752;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;731;560,-912;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;606;2495,576;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;742;-48,-1280;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;741;-48,-1280;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;732;560,-912;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;167;864,-1216;Inherit;True;Property;_ADDGreenDetailTexture;[ADD] Green Detail Texture:;13;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;489;2479,576;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;165;864,-1392;Inherit;True;Property;_ADDRedDetailTexture;[ADD] Red Detail Texture:;12;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;168;1200,-1216;Float;True;ColorZone2Detail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;488;863,576;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;116;-449,-528;Inherit;False;618.8059;1581.453;Grabbing Data From Split & User Etc;9;172;79;119;173;66;118;69;209;210;;1,0,0,1;0;0
Node;AmplifyShaderEditor.WireNode;735;560,-1072;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;166;1200,-1392;Float;True;colorZone1Detail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;738;272,-1280;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;67;368,-1120;Float;True;ControlR;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;73;608,-1120;Float;True;ControlG;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;172;-433,80;Inherit;True;166;colorZone1Detail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;591;847,576;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;118;-433,-96;Float;False;Property;_Color;Colour Red Channel;6;0;Create;False;0;0;False;0;False;1,0,0,0;0.6037736,0.6037736,0.6037736,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;119;-433,480;Float;False;Property;_ColourGreenChannel;Colour Green Channel;7;0;Create;True;0;0;False;0;False;0.07799447,1,0,0;0.1792451,0.1792451,0.1792451,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;68;32,-1488;Float;True;Diffuse;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;737;272,-1280;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;173;-433,656;Inherit;True;168;ColorZone2Detail;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;66;-433,-288;Inherit;True;67;ControlR;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;79;-433,272;Inherit;True;73;ControlG;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;592;847,560;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;209;-128,-96;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;210;-112,480;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;69;-433,-496;Inherit;True;68;Diffuse;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;662;320,-1488;Float;True;DiffuseAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;114;1520,-1472;Inherit;False;3721.344;882.909;Data contained in the Alpha of the diffuse, this is where we are going to approximate some data for Unity Specular. Smoothness was not present in Reach, and unity's Specular is not the same as Reach, so we're making our own.;27;428;429;636;21;100;20;122;99;667;668;669;673;676;677;681;690;692;710;720;721;722;723;724;717;689;719;718;Specular And Smoothness;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;594;847,320;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;81;207,352;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;64;191,-144;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;122;1696,-1120;Inherit;True;662;DiffuseAlpha;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;211;1567,-448;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;56;-480,1552;Inherit;False;1787.543;574.5555;For Normal Maps, *NOTE* that the default format of Halo Normal Maps is for Direct X. Unity uses OPENGL Rendering, where the Green channel is used differently. INVERT GREEN CHANNEL TO FIX IT FOR UNITY!;11;301;302;11;14;138;221;224;511;223;220;7;Normals;0,0.6689658,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;676;2080,-1120;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;1.949779;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;590;847,304;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;660;2560,64;Inherit;False;889.7329;377.3918;These Ensure players don't look flat when shaders are hidden;4;657;656;658;659;Fallback Properties;1,0,0,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;82;447,240;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;213;1583,-448;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;7;-448,1600;Inherit;True;Property;_BumpMap;NormalMap;2;1;[Normal];Create;False;0;0;False;0;False;-1;None;032d4b51a9b39d54eaa6411a52dda11c;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;380;1936,160;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;681;2320,-1120;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0.5;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;205;895,240;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;700;-480,2176;Inherit;False;1770.988;662.4712;Shading Stuff;7;697;693;699;701;702;705;706;Occlusion Map;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;657;2688,320;Float;False;Constant;_Glossiness;Glossiness;19;0;Create;True;0;0;False;0;False;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;656;2688,224;Float;False;Constant;_Metallic;Metallic;19;0;Create;True;0;0;False;0;False;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;718;2544,-976;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;659;2958,268;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;569;1968,128;Inherit;True;Multiply;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;693;-336,2304;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;220;96,1936;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;739;-96,-1232;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;658;3104,128;Inherit;True;Multiply;True;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;128;3552,80;Inherit;False;478.1267;151.4682;Export Finished Colouring For Use Elsewhere in the Shader;1;97;;0,1,0.2551723,1;0;0
Node;AmplifyShaderEditor.WireNode;223;96,1936;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;719;2544,-976;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;705;96,2304;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.75;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;699;-32,2544;Inherit;True;Property;_OcclusionMap;Occlusion Map;16;0;Create;True;0;0;False;0;False;-1;None;a71b28f2253a5dd468143f76fbc4dcbc;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;701;400,2304;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;740;-96,-1232;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;720;2544,-704;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;511;96,1936;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;97;3680,128;Float;False;FiysColourMethod;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;721;2544,-704;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;702;768,2528;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;320;4144,1536;Inherit;False;1132.072;1213.781;Where it's all coming Together;14;572;573;309;507;508;310;637;698;707;709;708;0;711;712;OUTPUT;0.7389706,0.9852941,0.9343306,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;668;1536,-1408;Inherit;True;97;FiysColourMethod;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;724;2032,-896;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LinearToGammaNode;664;320,-1264;Inherit;False;0;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;224;288,1936;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;736;528,-1280;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;221;288,1936;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;722;3616,-704;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;138;128,1728;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;717;2032,-896;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;20;3056,-864;Float;False;Property;_SmoothnessMultiplier;Smoothness Multiplier ;5;0;Create;True;0;0;False;0;False;1;1;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;697;1024,2304;Inherit;True;VarAmbientOcclusion;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;667;1968,-1392;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;1.949779,1.949779,1.949779,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FresnelNode;707;4160,2464;Inherit;True;Standard;WorldNormal;ViewDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0.33;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;669;2320,-1392;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;1.949779;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;14;432,1680;Inherit;True;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;723;3616,-704;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;708;4448,2464;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;3328,-944;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;692;2608,-1120;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;698;4176,2272;Inherit;True;697;VarAmbientOcclusion;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;665;576,-1360;Float;True;DiffuseAlphaGamma;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;677;2864,-1184;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;710;3696,-944;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;689;2800,-1392;Inherit;True;665;DiffuseAlphaGamma;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;709;4656,2272;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;636;4352,-1408;Inherit;False;598.9971;762.035;These Clamps are here to Prevent Artifacting, due to Blown out Values;2;570;571;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;180;-480,2928;Inherit;False;1789.95;466.3504;Maps for lighting on the Armor;5;144;143;131;509;510;Emmission;0.7389706,0.9852941,0.9343306,1;0;0
Node;AmplifyShaderEditor.ToggleSwitchNode;11;768,1600;Float;True;Property;_HaloReachDirectXNormalMap;Halo Reach (Direct X) Normal Map?;3;0;Create;True;0;0;False;0;False;0;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;302;1008,1632;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;131;-448,2992;Inherit;True;Property;_EmissionMap;[ADD] Emission Texture:;10;0;Create;False;0;0;False;0;False;-1;None;None;True;0;False;black;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;99;3472,-1232;Float;False;Property;_SpecularMultiplier;Specular Multiplier;4;0;Create;True;0;0;False;0;False;1;1;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;690;3280,-1312;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;143;-144,3152;Float;False;Property;_EmissionColor;Emission Color;11;0;Create;False;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;570;4512,-944;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;711;4848,2272;Inherit;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;144;-48,3024;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;301;1008,1632;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;637;4175.441,1803.788;Inherit;False;485.7451;455.2663;Look In "Metallic And Smoothness" Section for Source;3;574;432;433;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;429;5024,-944;Float;False;VarSmoothness;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;100;3760,-1312;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;712;5008,2272;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0.05,0.05,0.05,0;False;2;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;673;4064,-1312;Inherit;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;433;4208,2064;Inherit;True;429;VarSmoothness;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;716;5232,2256;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;510;1248,3056;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;303;1776,1632;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;509;1248,3056;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;305;1776,1632;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;715;5232,2256;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;574;4416,2096;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;571;4512,-1312;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0.05,0.05,0.05,0;False;2;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;428;5040,-1328;Float;False;VarSpecular;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;309;4208,1632;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;507;4208,1664;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;573;4736,1856;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;713;4848,1904;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;638;3616,1504;Inherit;False;332.8372;123.5029;Check Colouring Section for Source;1;98;;0,1,0.2551723,1;0;0
Node;AmplifyShaderEditor.WireNode;727;288,-656;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;279;560,-656;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;280;560,-656;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;728;288,-656;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;729;320,-880;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;730;320,-880;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;611;288,-848;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;608;288,-848;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;98;3664,1552;Inherit;False;97;FiysColourMethod;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;706;-128,2304;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;121;352,-864;Float;True;ControlBlue;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;332;864,-864;Inherit;True;Property;_ADDVisorDetailAlbedo;[ADD] Visor Detail Albedo;15;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;534;1615,688;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;610;-208,-1168;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;609;-208,-1168;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;714;4848,1904;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;432;4208,1856;Inherit;True;428;VarSpecular;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;572;4736,1856;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;310;4208,1632;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;123;608,-816;Float;True;ControlAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;508;4208,1664;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;331;1200,-864;Float;True;VisorDetail;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;5040,1584;Float;False;True;-1;2;ASEMaterialInspector;0;0;StandardSpecular;Open Halo Reach/Open Halo Reach Spartan Shader Spec V0.8;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;Standard (Specular setup);-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;253;-496,-1760;Inherit;False;361.9877;100;GNU LESSER GENERAL PUBLIC LICENSE                        Version 2.1, February 1999   Copyright (C) 1991, 1999 Free Software Foundation, Inc.  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  Everyone is permitted to copy and distribute verbatim copies  of this license document, but changing it is not allowed.  (This is the first released version of the Lesser GPL.  It also counts  as the successor of the GNU Library Public License, version 2, hence  the version number 2.1.)                              Preamble    The licenses for most software are designed to take away your freedom to share and change it.  By contrast, the GNU General Public Licenses are intended to guarantee your freedom to share and change free software--to make sure the software is free for all its users.    This license, the Lesser General Public License, applies to some specially designated software packages--typically libraries--of the Free Software Foundation and other authors who decide to use it.  You can use it too, but we suggest you first think carefully about whether this license or the ordinary General Public License is the better strategy to use in any particular case, based on the explanations below.    When we speak of free software, we are referring to freedom of use, not price.  Our General Public Licenses are designed to make sure that you have the freedom to distribute copies of free software (and charge for this service if you wish)  that you receive source code or can get it if you want it  that you can change the software and use pieces of it in new free programs  and that you are informed that you can do these things.    To protect your rights, we need to make restrictions that forbid distributors to deny you these rights or to ask you to surrender these rights.  These restrictions translate to certain responsibilities for you if you distribute copies of the library or if you modify it.    For example, if you distribute copies of the library, whether gratis or for a fee, you must give the recipients all the rights that we gave you.  You must make sure that they, too, receive or can get the source code.  If you link other code with the library, you must provide complete object files to the recipients, so that they can relink them with the library after making changes to the library and recompiling it.  And you must show them these terms so they know their rights.    We protect your rights with a two-step method: (1) we copyright the library, and (2) we offer you this license, which gives you legal permission to copy, distribute and/or modify the library.    To protect each distributor, we want to make it very clear that there is no warranty for the free library.  Also, if the library is modified by someone else and passed on, the recipients should know that what they have is not the original version, so that the original author's reputation will not be affected by problems that might be introduced by others.    Finally, software patents pose a constant threat to the existence of any free program.  We wish to make sure that a company cannot effectively restrict the users of a free program by obtaining a restrictive license from a patent holder.  Therefore, we insist that any patent license obtained for a version of the library must be consistent with the full freedom of use specified in this license.    Most GNU software, including some libraries, is covered by the ordinary GNU General Public License.  This license, the GNU Lesser General Public License, applies to certain designated libraries, and is quite different from the ordinary General Public License.  We use this license for certain libraries in order to permit linking those libraries into non-free programs.    When a program is linked with a library, whether statically or using a shared library, the combination of the two is legally speaking a combined work, a derivative of the original library.  The ordinary General Public License therefore permits such linking only if the entire combination fits its criteria of freedom.  The Lesser General Public License permits more lax criteria for linking other code with the library.    We call this license the "Lesser" General Public License because it does Less to protect the user's freedom than the ordinary General Public License.  It also provides other free software developers Less of an advantage over competing non-free programs.  These disadvantages are the reason we use the ordinary General Public License for many libraries.  However, the Lesser license provides advantages in certain special circumstances.    For example, on rare occasions, there may be a special need to encourage the widest possible use of a certain library, so that it becomes a de-facto standard.  To achieve this, non-free programs must be allowed to use the library.  A more frequent case is that a free library does the same job as widely used non-free libraries.  In this case, there is little to gain by limiting the free library to free software only, so we use the Lesser General Public License.    In other cases, permission to use a particular library in non-free programs enables a greater number of people to use a large body of free software.  For example, permission to use the GNU C Library in non-free programs enables many more people to use the whole GNU operating system, as well as its variant, the GNU/Linux operating system.    Although the Lesser General Public License is Less protective of the users' freedom, it does ensure that the user of a program that is linked with the Library has the freedom and the wherewithal to run that program using a modified version of the Library.    The precise terms and conditions for copying, distribution and modification follow.  Pay close attention to the difference between a "work based on the library" and a "work that uses the library".  The former contains code derived from the library, whereas the latter must be combined with the library in order to run.                    GNU LESSER GENERAL PUBLIC LICENSE    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION    0. This License Agreement applies to any software library or other program which contains a notice placed by the copyright holder or other authorized party saying it may be distributed under the terms of this Lesser General Public License (also called "this License"). Each licensee is addressed as "you".    A "library" means a collection of software functions and/or data prepared so as to be conveniently linked with application programs (which use some of those functions and data) to form executables.    The "Library", below, refers to any such software library or work which has been distributed under these terms.  A "work based on the Library" means either the Library or any derivative work under copyright law: that is to say, a work containing the Library or a portion of it, either verbatim or with modifications and/or translated straightforwardly into another language.  (Hereinafter, translation is included without limitation in the term "modification".)    "Source code" for a work means the preferred form of the work for making modifications to it.  For a library, complete source code means all the source code for all modules it contains, plus any associated interface definition files, plus the scripts used to control compilation and installation of the library.    Activities other than copying, distribution and modification are not covered by this License  they are outside its scope.  The act of running a program using the Library is not restricted, and output from such a program is covered only if its contents constitute a work based on the Library (independent of the use of the Library in a tool for writing it).  Whether that is true depends on what the Library does and what the program that uses the Library does.    1. You may copy and distribute verbatim copies of the Library's complete source code as you receive it, in any medium, provided that you conspicuously and appropriately publish on each copy an appropriate copyright notice and disclaimer of warranty  keep intact all the notices that refer to this License and to the absence of any warranty  and distribute a copy of this License along with the Library.    You may charge a fee for the physical act of transferring a copy, and you may at your option offer warranty protection in exchange for a fee.    2. You may modify your copy or copies of the Library or any portion of it, thus forming a work based on the Library, and copy and distribute such modifications or work under the terms of Section 1 above, provided that you also meet all of these conditions:      a) The modified work must itself be a software library.      b) You must cause the files modified to carry prominent notices     stating that you changed the files and the date of any change.      c) You must cause the whole of the work to be licensed at no     charge to all third parties under the terms of this License.      d) If a facility in the modified Library refers to a function or a     table of data to be supplied by an application program that uses     the facility, other than as an argument passed when the facility     is invoked, then you must make a good faith effort to ensure that,     in the event an application does not supply such function or     table, the facility still operates, and performs whatever part of     its purpose remains meaningful.      (For example, a function in a library to compute square roots has     a purpose that is entirely well-defined independent of the     application.  Therefore, Subsection 2d requires that any     application-supplied function or table used by this function must     be optional: if the application does not supply it, the square     root function must still compute square roots.)  These requirements apply to the modified work as a whole.  If identifiable sections of that work are not derived from the Library, and can be reasonably considered independent and separate works in themselves, then this License, and its terms, do not apply to those sections when you distribute them as separate works.  But when you distribute the same sections as part of a whole which is a work based on the Library, the distribution of the whole must be on the terms of this License, whose permissions for other licensees extend to the entire whole, and thus to each and every part regardless of who wrote it.  Thus, it is not the intent of this section to claim rights or contest your rights to work written entirely by you  rather, the intent is to exercise the right to control the distribution of derivative or collective works based on the Library.  In addition, mere aggregation of another work not based on the Library with the Library (or with a work based on the Library) on a volume of a storage or distribution medium does not bring the other work under the scope of this License.    3. You may opt to apply the terms of the ordinary GNU General Public License instead of this License to a given copy of the Library.  To do this, you must alter all the notices that refer to this License, so that they refer to the ordinary GNU General Public License, version 2, instead of to this License.  (If a newer version than version 2 of the ordinary GNU General Public License has appeared, then you can specify that version instead if you wish.)  Do not make any other change in these notices.    Once this change is made in a given copy, it is irreversible for that copy, so the ordinary GNU General Public License applies to all subsequent copies and derivative works made from that copy.    This option is useful when you wish to copy part of the code of the Library into a program that is not a library.    4. You may copy and distribute the Library (or a portion or derivative of it, under Section 2) in object code or executable form under the terms of Sections 1 and 2 above provided that you accompany it with the complete corresponding machine-readable source code, which must be distributed under the terms of Sections 1 and 2 above on a medium customarily used for software interchange.    If distribution of object code is made by offering access to copy from a designated place, then offering equivalent access to copy the source code from the same place satisfies the requirement to distribute the source code, even though third parties are not compelled to copy the source along with the object code.    5. A program that contains no derivative of any portion of the Library, but is designed to work with the Library by being compiled or linked with it, is called a "work that uses the Library".  Such a work, in isolation, is not a derivative work of the Library, and therefore falls outside the scope of this License.    However, linking a "work that uses the Library" with the Library creates an executable that is a derivative of the Library (because it contains portions of the Library), rather than a "work that uses the library".  The executable is therefore covered by this License. Section 6 states terms for distribution of such executables.    When a "work that uses the Library" uses material from a header file that is part of the Library, the object code for the work may be a derivative work of the Library even though the source code is not. Whether this is true is especially significant if the work can be linked without the Library, or if the work is itself a library.  The threshold for this to be true is not precisely defined by law.    If such an object file uses only numerical parameters, data structure layouts and accessors, and small macros and small inline functions (ten lines or less in length), then the use of the object file is unrestricted, regardless of whether it is legally a derivative work.  (Executables containing this object code plus portions of the Library will still fall under Section 6.)    Otherwise, if the work is a derivative of the Library, you may distribute the object code for the work under the terms of Section 6. Any executables containing that work also fall under Section 6, whether or not they are linked directly with the Library itself.    6. As an exception to the Sections above, you may also combine or link a "work that uses the Library" with the Library to produce a work containing portions of the Library, and distribute that work under terms of your choice, provided that the terms permit modification of the work for the customer's own use and reverse engineering for debugging such modifications.    You must give prominent notice with each copy of the work that the Library is used in it and that the Library and its use are covered by this License.  You must supply a copy of this License.  If the work during execution displays copyright notices, you must include the copyright notice for the Library among them, as well as a reference directing the user to the copy of this License.  Also, you must do one of these things:      a) Accompany the work with the complete corresponding     machine-readable source code for the Library including whatever     changes were used in the work (which must be distributed under     Sections 1 and 2 above)  and, if the work is an executable linked     with the Library, with the complete machine-readable "work that     uses the Library", as object code and/or source code, so that the     user can modify the Library and then relink to produce a modified     executable containing the modified Library.  (It is understood     that the user who changes the contents of definitions files in the     Library will not necessarily be able to recompile the application     to use the modified definitions.)      b) Use a suitable shared library mechanism for linking with the     Library.  A suitable mechanism is one that (1) uses at run time a     copy of the library already present on the user's computer system,     rather than copying library functions into the executable, and (2)     will operate properly with a modified version of the library, if     the user installs one, as long as the modified version is     interface-compatible with the version that the work was made with.      c) Accompany the work with a written offer, valid for at     least three years, to give the same user the materials     specified in Subsection 6a, above, for a charge no more     than the cost of performing this distribution.      d) If distribution of the work is made by offering access to copy     from a designated place, offer equivalent access to copy the above     specified materials from the same place.      e) Verify that the user has already received a copy of these     materials or that you have already sent this user a copy.    For an executable, the required form of the "work that uses the Library" must include any data and utility programs needed for reproducing the executable from it.  However, as a special exception, the materials to be distributed need not include anything that is normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which the executable runs, unless that component itself accompanies the executable.    It may happen that this requirement contradicts the license restrictions of other proprietary libraries that do not normally accompany the operating system.  Such a contradiction means you cannot use both them and the Library together in an executable that you distribute.    7. You may place library facilities that are a work based on the Library side-by-side in a single library together with other library facilities not covered by this License, and distribute such a combined library, provided that the separate distribution of the work based on the Library and of the other library facilities is otherwise permitted, and provided that you do these two things:      a) Accompany the combined library with a copy of the same work     based on the Library, uncombined with any other library     facilities.  This must be distributed under the terms of the     Sections above.      b) Give prominent notice with the combined library of the fact     that part of it is a work based on the Library, and explaining     where to find the accompanying uncombined form of the same work.    8. You may not copy, modify, sublicense, link with, or distribute the Library except as expressly provided under this License.  Any attempt otherwise to copy, modify, sublicense, link with, or distribute the Library is void, and will automatically terminate your rights under this License.  However, parties who have received copies, or rights, from you under this License will not have their licenses terminated so long as such parties remain in full compliance.    9. You are not required to accept this License, since you have not signed it.  However, nothing else grants you permission to modify or distribute the Library or its derivative works.  These actions are prohibited by law if you do not accept this License.  Therefore, by modifying or distributing the Library (or any work based on the Library), you indicate your acceptance of this License to do so, and all its terms and conditions for copying, distributing or modifying the Library or works based on it.    10. Each time you redistribute the Library (or any work based on the Library), the recipient automatically receives a license from the original licensor to copy, distribute, link with or modify the Library subject to these terms and conditions.  You may not impose any further restrictions on the recipients' exercise of the rights granted herein. You are not responsible for enforcing compliance by third parties with this License.    11. If, as a consequence of a court judgment or allegation of patent infringement or for any other reason (not limited to patent issues), conditions are imposed on you (whether by court order, agreement or otherwise) that contradict the conditions of this License, they do not excuse you from the conditions of this License.  If you cannot distribute so as to satisfy simultaneously your obligations under this License and any other pertinent obligations, then as a consequence you may not distribute the Library at all.  For example, if a patent license would not permit royalty-free redistribution of the Library by all those who receive copies directly or indirectly through you, then the only way you could satisfy both it and this License would be to refrain entirely from distribution of the Library.  If any portion of this section is held invalid or unenforceable under any particular circumstance, the balance of the section is intended to apply, and the section as a whole is intended to apply in other circumstances.  It is not the purpose of this section to induce you to infringe any patents or other property right claims or to contest validity of any such claims  this section has the sole purpose of protecting the integrity of the free software distribution system which is implemented by public license practices.  Many people have made generous contributions to the wide range of software distributed through that system in reliance on consistent application of that system  it is up to the author/donor to decide if he or she is willing to distribute software through any other system and a licensee cannot impose that choice.  This section is intended to make thoroughly clear what is believed to be a consequence of the rest of this License.    12. If the distribution and/or use of the Library is restricted in certain countries either by patents or by copyrighted interfaces, the original copyright holder who places the Library under this License may add an explicit geographical distribution limitation excluding those countries, so that distribution is permitted only in or among countries not thus excluded.  In such case, this License incorporates the limitation as if written in the body of this License.    13. The Free Software Foundation may publish revised and/or new versions of the Lesser General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.  Each version is given a distinguishing version number.  If the Library specifies a version number of this License which applies to it and "any later version", you have the option of following the terms and conditions either of that version or of any later version published by the Free Software Foundation.  If the Library does not specify a license version number, you may choose any version ever published by the Free Software Foundation.    14. If you wish to incorporate parts of the Library into other free programs whose distribution conditions are incompatible with these, write to the author to ask for permission.  For software which is copyrighted by the Free Software Foundation, write to the Free Software Foundation  we sometimes make exceptions for this.  Our decision will be guided by the two goals of preserving the free status of all derivatives of our free software and of promoting the sharing and reuse of software generally.                              NO WARRANTY    15. BECAUSE THE LIBRARY IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR THE LIBRARY, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE LIBRARY "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE LIBRARY IS WITH YOU.  SHOULD THE LIBRARY PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.    16. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR REDISTRIBUTE THE LIBRARY AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE LIBRARY (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE LIBRARY TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.                       END OF TERMS AND CONDITIONS             How to Apply These Terms to Your New Libraries    If you develop a new library, and you want it to be of the greatest possible use to the public, we recommend making it free software that everyone can redistribute and change.  You can do so by permitting redistribution under these terms (or, alternatively, under the terms of the ordinary General Public License).    To apply these terms, attach the following notices to the library.  It is safest to attach them to the start of each source file to most effectively convey the exclusion of warranty  and each file should have at least the "copyright" line and a pointer to where the full notice is found.      shaders     Copyright (C) 2019 John W      This library is free software  you can redistribute it and/or     modify it under the terms of the GNU Lesser General Public     License as published by the Free Software Foundation  either     version 2.1 of the License, or (at your option) any later version.      This library is distributed in the hope that it will be useful,     but WITHOUT ANY WARRANTY  without even the implied warranty of     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     Lesser General Public License for more details.      You should have received a copy of the GNU Lesser General Public     License along with this library  if not, write to the Free Software     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301     USA  Also add information on how to contact you by electronic and paper mail.  You should also get your employer (if you work as a programmer) or your school, if any, to sign a "copyright disclaimer" for the library, if necessary.  Here is a sample  alter the names:    Yoyodyne, Inc., hereby disclaims all copyright interest in the   library `Frob' (a library for tweaking knobs) written by James Random   Hacker.    {signature of Ty Coon}, 1 April 1990   Ty Coon, President of Vice  That's all there is to it! ;0;This Shader is Licensed under the LGPL License Please read the associated Text File.;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;129;-480,-1968;Inherit;False;100;100;...;0;Created By FiyCsf For Use With Halo Reach Spartan Models;1,1,1,1;0;0
WireConnection;527;0;1;0
WireConnection;745;0;527;0
WireConnection;744;0;745;0
WireConnection;746;0;744;0
WireConnection;743;0;746;0
WireConnection;528;0;743;0
WireConnection;536;0;528;1
WireConnection;535;0;528;0
WireConnection;170;0;171;0
WireConnection;85;0;208;0
WireConnection;85;1;207;0
WireConnection;533;0;85;0
WireConnection;526;0;533;0
WireConnection;661;0;120;0
WireConnection;661;1;174;0
WireConnection;94;0;526;0
WireConnection;94;1;661;0
WireConnection;96;0;526;0
WireConnection;96;1;94;0
WireConnection;607;0;96;0
WireConnection;605;0;607;0
WireConnection;733;0;1;2
WireConnection;734;0;733;0
WireConnection;604;0;605;0
WireConnection;731;0;734;0
WireConnection;606;0;604;0
WireConnection;742;0;6;4
WireConnection;741;0;742;0
WireConnection;732;0;731;0
WireConnection;489;0;606;0
WireConnection;168;0;167;0
WireConnection;488;0;489;0
WireConnection;735;0;732;0
WireConnection;166;0;165;0
WireConnection;738;0;741;0
WireConnection;67;0;1;1
WireConnection;73;0;735;0
WireConnection;591;0;488;0
WireConnection;68;0;6;0
WireConnection;737;0;738;0
WireConnection;592;0;591;0
WireConnection;209;0;118;0
WireConnection;209;1;172;0
WireConnection;210;0;119;0
WireConnection;210;1;173;0
WireConnection;662;0;737;0
WireConnection;594;0;592;0
WireConnection;81;0;79;0
WireConnection;81;1;210;0
WireConnection;64;0;66;0
WireConnection;64;1;209;0
WireConnection;211;0;69;0
WireConnection;676;0;122;0
WireConnection;676;1;122;0
WireConnection;590;0;594;0
WireConnection;82;0;64;0
WireConnection;82;1;81;0
WireConnection;213;0;211;0
WireConnection;380;0;213;0
WireConnection;681;0;676;0
WireConnection;681;1;676;0
WireConnection;681;2;676;0
WireConnection;205;0;82;0
WireConnection;205;1;590;0
WireConnection;718;0;681;0
WireConnection;659;0;656;0
WireConnection;659;1;657;0
WireConnection;569;0;380;0
WireConnection;569;1;205;0
WireConnection;693;0;7;3
WireConnection;693;1;7;3
WireConnection;693;2;7;3
WireConnection;220;0;7;3
WireConnection;739;0;6;4
WireConnection;658;0;569;0
WireConnection;658;1;659;0
WireConnection;223;0;220;0
WireConnection;719;0;718;0
WireConnection;705;0;693;0
WireConnection;701;0;705;0
WireConnection;701;1;699;0
WireConnection;740;0;739;0
WireConnection;720;0;719;0
WireConnection;511;0;223;0
WireConnection;97;0;658;0
WireConnection;721;0;720;0
WireConnection;702;0;701;0
WireConnection;702;1;699;0
WireConnection;724;0;122;0
WireConnection;664;0;740;0
WireConnection;224;0;511;0
WireConnection;736;0;664;0
WireConnection;221;0;224;0
WireConnection;722;0;721;0
WireConnection;138;0;7;2
WireConnection;717;0;724;0
WireConnection;697;0;702;0
WireConnection;667;0;668;0
WireConnection;669;0;667;0
WireConnection;669;1;122;0
WireConnection;14;0;7;1
WireConnection;14;1;138;0
WireConnection;14;2;221;0
WireConnection;723;0;722;0
WireConnection;708;0;707;0
WireConnection;21;0;717;0
WireConnection;21;1;20;0
WireConnection;692;0;681;0
WireConnection;665;0;736;0
WireConnection;677;0;669;0
WireConnection;677;1;692;0
WireConnection;710;0;21;0
WireConnection;710;1;723;0
WireConnection;709;0;698;0
WireConnection;709;1;708;0
WireConnection;11;0;7;0
WireConnection;11;1;14;0
WireConnection;302;0;11;0
WireConnection;690;0;689;0
WireConnection;690;1;677;0
WireConnection;570;0;710;0
WireConnection;711;0;709;0
WireConnection;144;0;131;0
WireConnection;144;1;143;0
WireConnection;301;0;302;0
WireConnection;429;0;570;0
WireConnection;100;0;690;0
WireConnection;100;1;99;0
WireConnection;712;0;711;0
WireConnection;673;0;100;0
WireConnection;716;0;712;0
WireConnection;510;0;144;0
WireConnection;303;0;301;0
WireConnection;509;0;510;0
WireConnection;305;0;303;0
WireConnection;715;0;716;0
WireConnection;574;0;433;0
WireConnection;571;0;673;0
WireConnection;428;0;571;0
WireConnection;309;0;305;0
WireConnection;507;0;509;0
WireConnection;573;0;574;0
WireConnection;713;0;715;0
WireConnection;727;0;608;0
WireConnection;279;0;728;0
WireConnection;280;0;279;0
WireConnection;728;0;727;0
WireConnection;729;0;1;3
WireConnection;730;0;729;0
WireConnection;611;0;610;0
WireConnection;608;0;611;0
WireConnection;706;0;693;0
WireConnection;706;1;693;0
WireConnection;121;0;730;0
WireConnection;534;0;526;0
WireConnection;610;0;609;0
WireConnection;609;0;1;4
WireConnection;714;0;713;0
WireConnection;572;0;573;0
WireConnection;310;0;309;0
WireConnection;123;0;280;0
WireConnection;508;0;507;0
WireConnection;331;0;332;0
WireConnection;0;0;98;0
WireConnection;0;1;310;0
WireConnection;0;2;508;0
WireConnection;0;3;432;0
WireConnection;0;4;572;0
WireConnection;0;5;714;0
ASEEND*/
//CHKSM=8933AC2B2E7F8A786CEAE226D6F4EBA1A1195698
